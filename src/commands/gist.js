module.exports = {
  name: "gist",
  desc: "Get code from GitHub Gist and run.",
  execute(message, axios, args) {
    !args[0]
      ? message.reply("Correct use: `$gist gist_id`")
      : axios
          .get(`https://api.github.com/gists/${args[0]}`)
          .then(async (results) => {
            const msg = await message.channel.send(`Waiting...`);
            let rdata = results.data.files; //Result object
            let keys = Object.keys(rdata).toString(); // get keys from object
            let rgx = /[^.]+$/; // [^.]+$ regex to get text after dot char
            let lang = rgx.exec(keys)[0];
            let code = rdata[keys].content;
            let data = JSON.stringify({
              code: code,
              language: lang,
              input: `Test Input`,
            });

            let config = {
              method: "post",
              url: "https://codexweb.netlify.app/.netlify/functions/enforceCode",
              headers: {
                "Content-Type": "application/json",
              },
              data: data,
            };

            axios(config)
              .then(function (response) {
                msg
                  .edit(
                    `\`\`\`${lang}\n${response.data.output}\n\nerrorCode: ${response.data.errorCode}\`\`\``
                  )
                  .catch((e) => {
                    if (e.code == 50035) {
                      msg.edit("Your output was too long :<");
                    }
                  });
              })
              .catch(function (error) {
                msg.edit(`Oh no! Got an error: ${error}`);
              });
          })
          .catch((err) => {
            console.log(err);
          });
  },
};
