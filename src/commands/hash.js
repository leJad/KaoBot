module.exports = {
  name: "hash",
  desc: "Hashes text with a given algorithm.",
  execute(message, crypto, args) {
    try {
      let hash = args.shift();
      let output = crypto
        .createHash(hash.toLowerCase())
        .update(args.join(" "))
        .digest("hex");
      message.channel.send(`${hash}: ${output}`);
    } catch (e) {
      message.react("❔");
    }
  },
};
