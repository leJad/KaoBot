module.exports = {
  name: "cipher",
  desc: "Creates a Cipher object using the specific algorithm",
  execute(message, crypto, args) {
    const algorithm = args.shift(); //User will give us the algorithm

    const key = crypto.randomBytes(32); // Create key, required

    const iv = crypto.randomBytes(16); // Create iv, required

    // Func start
    function encrypt(text) {
      let cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(key), iv);

      let encrypted = cipher.update(text);

      encrypted = Buffer.concat([encrypted, cipher.final()]);

      return {
        iv: iv.toString("base64"),
        encryptedData: encrypted.toString("base64"),
      };
    }
    // Func end

    var output = encrypt(args.join(" ")); //Encrypting the data given from user
    message.channel.send(output.encryptedData); //Send encrypted data back
  },
};
