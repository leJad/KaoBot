module.exports = {
  name: "run",
  desc: "Run your code.",
  execute(message, args, axios) {
    !args[0]
      ? message.reply(
          "Please enter a programming lang. (Check $**help langs** for the language list)"
        )
      : message.reply("Please enter your code.").then(() => {
          const filter = (m) => message.author.id === m.author.id;

          message.channel
            .awaitMessages(filter, { time: 60000, max: 1, errors: ["time"] })
            .then(async (messages) => {
              const msg = await message.channel.send(`Waiting...`);
              let lang = args.shift();
              let kode = messages.first().content;
              let data = JSON.stringify({
                code: kode,
                language: lang,
                input: `Test Input`,
              });

              let config = {
                method: "post",
                url: "https://codexweb.netlify.app/.netlify/functions/enforceCode",
                headers: {
                  "Content-Type": "application/json",
                },
                data: data,
              };

              axios(config)
                .then(function (response) {
                  msg
                    .edit(
                      `\`\`\`${lang}\n${response.data.output}\n\nerrorCode: ${response.data.errorCode}\`\`\``
                    )
                    .catch((e) => {
                      if (e.code == 50035) {
                        msg.edit("Your output was too long :<");
                      }
                    });
                })
                .catch(function (error) {
                  msg.edit(`Oh no! Got an error: ${error}`);
                });
            })
            .catch(() => {
              message.channel.send("You did not enter any input!");
            });
        });
  },
};
