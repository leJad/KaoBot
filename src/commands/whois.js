module.exports = {
  name: "whois",
  desc: "whois info",
  execute(message, args, whois) {
    whois.lookup(args[0], function (err, data) {
      message.channel.send(data, { split: true }); //Continue to send message if the data is more than 2kB (2000 char)
    });
  },
};
