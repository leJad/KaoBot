module.exports = {
  name: "ping",
  desc: "Pong!",
  async execute(message, client) {
    const msg = await message.channel.send("Ping?");
    msg.edit(`🏓 ${client.ws.ping}ms.`);
  },
};
