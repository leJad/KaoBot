module.exports = {
  name: "btt",
  desc: "Binary to Text Converter",
  execute(message, args) {
    message.channel.send(
      args
        .join(" ")
        .split(" ")
        .map((elem) => String.fromCharCode(parseInt(elem, 2)))
        .join("")
    );
  },
};
