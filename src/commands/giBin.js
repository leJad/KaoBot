module.exports = {
  name: "paste",
  desc: "paste to gibin",
  execute(message, axios, args) {
    const params = new URLSearchParams();
    params.append("paste", args[0]);

    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };

    axios.post("https://gibin.lejad.repl.co/", params, config).then(
      (response) => {
        message.reply(`URL: ${response.request._redirectable._options.href}`); 
      },
      (error) => {
        console.log(error);
      }
    );
  },
};
