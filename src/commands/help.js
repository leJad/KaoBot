module.exports = {
  name: "help",
  desc: "Help command",
  execute(message, crypto, args, prefix) {
    let eray = message.content.split(" ");
    let hargs = eray.slice(1);

    if (hargs[0] === "langs" || hargs[0] === "lang") {
      message.reply(`\`\`\` Here is the list of languages that I support:
╔════════════╤═══════╗
║ Javascript │ js    ║
╠════════════╪═══════╣
║ Python     │ py    ║
╟────────────┼───────╢
║ Swift      │ swift ║
╟────────────┼───────╢
║ Java       │ java  ║
╟────────────┼───────╢
║ C          │ c     ║
╟────────────┼───────╢
║ C++        │ cpp   ║
╟────────────┼───────╢
║ C#         │ cs    ║
╟────────────┼───────╢
║ GoLang     │ go    ║
╟────────────┼───────╢
║ Kotlin     │ kt    ║
╟────────────┼───────╢
║ Ruby       │ rb    ║
╚════════════╧═══════╝
\`\`\``);
    } else if (hargs[0] === "hlist") {
      message.channel.send(crypto.getHashes());
    }
    if (!hargs[0]) {
      message.channel.send(`
        
        These are my supported commands:
    
        ${prefix}**ping** - Pong!,
        ${prefix}**run** <programming_language> - Run your code. Check $**help langs** to see the list of supported languages,
        ${prefix}**hash** <algorithm> <text> - Hashes text with a given algorithm. Check $**help hlist** to see the list of supported hash algorithms,
        ${prefix}**tbt** <text> - Text to Bit Converter,
        ${prefix}**btt** <text> - Bit to Text Converter,
        ${prefix}**cipher** <algorithm> <text> - Creates a Cipher object using the specific algorithm,
        ${prefix}**whois** <website_domain> - WHOIS info,
        ${prefix}**gist** <gist_id> - Get code from GitHub Gists and run.
        ${prefix}**paste** <message> - Paste your text to giBin. 

        `);
    }
  },
};
