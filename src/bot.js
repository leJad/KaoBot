const Discord = require("discord.js");
const client = new Discord.Client();
const axios = require("axios");
const fs = require("fs");
const crypto = require("crypto");
const whois = require("whois");
require("dotenv").config();

const prefix = process.env.PREFIX;

client.commands = new Discord.Collection();

const commandFiles = fs
  .readdirSync("./commands/")
  .filter((file) => file.endsWith(".js"));
for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  client.commands.set(command.name, command);
}

client.once("ready", () => {
  client.user.setActivity("$help", {
    type: "STREAMING",
    url: "https://www.twitch.tv/noimnotstreamingbro",
  });
  console.log("Ready!");
});

client.on("message", async (message) => {
  if (!message.content.startsWith(prefix) || message.author.bot) return;

  const args = message.content.slice(prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  // if (!client.commands.has(command)) return;

  switch (command) {
    case "ping":
      client.commands.get("ping").execute(message, client);
      break;
    case "run":
      client.commands.get("run").execute(message, args, axios);
      break;
    case "help":
      client.commands.get("help").execute(message, crypto, args, prefix);
      break;
    case "hash":
      client.commands.get("hash").execute(message, crypto, args);
      break;
    case "tbt":
      client.commands.get("tbt").execute(message, args);
      break;
    case "btt":
      client.commands.get("btt").execute(message, args);
      break;
    case "cipher":
      client.commands.get("cipher").execute(message, crypto, args);
      break;
    case "whois":
      client.commands.get("whois").execute(message, args, whois);
      break;
    case "gist":
      client.commands.get("gist").execute(message, axios, args);
      break;
    case "paste":
      client.commands.get("paste").execute(message, axios, args);
      break;
    default:
      message.channel.send("Unknown command!");
      break;
  }
});

// TODO: Add code saving command

client.login(process.env.TOKEN);
