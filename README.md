# KaoBot
Kao is a Discord bot that can compile your code and display the output of the compiler. It also provides some helpful tools like cipher, whois etc. 

[Invitation Link](https://discord.com/api/oauth2/authorize?client_id=841792548895850506&permissions=3963202641&scope=bot)

## Features

- Run your code directly from Discord
- Run code from GitHub Gists
- Supports more than 10 languages! 
- Hash text with given algorithm
- Get whois info of domain
- And More!

## Screenshots

![Screenshot 0](https://imgur.com/fvzMfWD.png)
![Screenshot 1](https://imgur.com/BZEl3D0.png)

## Run Locally

Clone the project

```bash
  git clone https://github.com/leJad/KaoBot.git
```

Go to the project directory

```bash
  cd KaoBot/src
```

Install dependencies

```bash
  npm install
```

To run this project, you will need to create, and add the following environment variables to your .env file. For more information check .env.example file

`TOKEN`

`PREFIX`

Start the bot

```bash
  node bot.js
```

## Feedback

If you have any feedback, please reach out to me at [ecma@gmail.com](mailto:stephanscode345@gmail.com)
 
